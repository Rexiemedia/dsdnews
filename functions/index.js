const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);

const createNotification = ((notification) => {
  return admin.firestore().collection('notifications')
    .add(notification)
    .then(doc => console.log('notification added'));
});

// app.use(require('prerender-node').set('prerenderToken', 'DHoOP6ByCq9ieeMTalmG'));
exports.projectCreated = functions.firestore
  .document('projects/{projectId}')
  .onCreate((snap, context) => {

    console.log('snapshot is:', snap);
    const projectId = context.params.projectId;
    console.log("params", projectId);
    const project = snap.data();
    const notification = {
      title: `${project.title}`,
      projectId: `${projectId}`,
      time: admin.firestore.FieldValue.serverTimestamp()
    }

    return createNotification(notification);

});

exports.userJoined = functions.auth.user()
  .onCreate(user => {
    
    return admin.firestore().collection('users')
      .doc(user.uid).get().then(doc => {

        const newUser = doc.data();
        const notification = {
          content: 'Joined the party',
          user: `${newUser.firstName} ${newUser.lastName}`,
          time: admin.firestore.FieldValue.serverTimestamp()
        };

        return createNotification(notification);

      });
});
