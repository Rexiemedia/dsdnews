 import firebase from 'firebase/app'
 import 'firebase/firestore'
 import 'firebase/auth'
 import 'firebase/storage'

 // Initialize Firebase
 var config = {
  apiKey: "AIzaSyDENNbfEZIueIgi5JT_o1jJSteJV7dm7Go",
  authDomain: "dsdnewsapi.firebaseapp.com",
  databaseURL: "https://dsdnewsapi.firebaseio.com",
  projectId: "dsdnewsapi",
  storageBucket: "dsdnewsapi.appspot.com",
  messagingSenderId: "935537675204"
};
  
firebase.initializeApp(config);
firebase.firestore().settings({ timestampsInSnapshots: true });

export default firebase