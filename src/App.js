import React, { Component } from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import Navbar from './components/layouts/Navbar'
import Footer from './components/layouts/Footer';
import Dashboard from './components/dashboard/Dashboard'
import HomeComponent from './components/home/HomeComponent'
import ProjectDetails from './components/projects/ProjectDetails'
import Signin from './components/auth/SignIn'
import Signup from './components/auth/SignUp'
import CreateProject from './components/projects/CreateProject'
import UpdateProject from './components/projects/UpdateProject'
import CrimeComponent from './components/home/CrimeWatch'
import EntertainmentComponent from './components/home/Entertainment'
import PoliticsComponent from './components/home/Highlight'
import SportsComponent from './components/home/Sports'
import LatestComponent from './components/home/LatestNews'
import Contact from './components/contact/Contact'


class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div className="App">
          <Navbar />
          <Switch>
            <Route exact path='/' component={HomeComponent} />
            <Route path='/all_cat' component={LatestComponent} />
            <Route path='/dashboard' component={Dashboard} />
            <Route path='/project/:id' component={ProjectDetails} />
            <Route path='/signup' component={Signup} />
            <Route path='/signin' component={Signin} />
            <Route path='/createproject' component={CreateProject} />
            <Route path='/updateproject' component={UpdateProject} />
            <Route path='/crime-watch' component={CrimeComponent} />
            <Route path='/entertainment' component={EntertainmentComponent} />
            <Route path='/politics' component={PoliticsComponent} />
            <Route path='/sports' component={SportsComponent} />
            <Route path='/contactus' component={Contact} />
            <Route path='**/**' component={HomeComponent} />
          </Switch>
          <Footer />
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
