import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'
import { connect } from 'react-redux'
import { signUp } from '../store/actions/authActions'
import { Checkbox } from 'antd'
import firebase from '../../config/fbConfig'
import FileUploader from "react-firebase-file-uploader"


class SignUp extends Component {
  state = {
    email: '',
    password: '',
    firstName: '',
    lastName: '',
    avatar: '',
    photoURL: '',
    active: false
  }
  
  handleChange = (e) => {
    this.setState({
      [e.target.id]: e.target.value,
    })
  }

  handleChanges = (e) => {
    this.accepted = e.target.checked;
  }

  handleUploadSuccess = filename => {
    this.setState({ 
      avatar: filename, 
      progress: 100 
    });

    firebase.storage().ref('profilePics').child(filename).getDownloadURL()
      .then(url => this.setState({ photoURL: url }));
  }


  handleSubmit = (e) => {
    e.preventDefault();

    const {firstName, lastName, email } = this.state;
    if (this.accepted === undefined || this.accepted === false) {
      return;
    }
    else if(!this.validateFirstname(firstName) || !this.validateLastname(lastName)){
      return; 
    }
    else if(!this.validateEmail(email)){
      return;
    }
    // processing input
    this.props.signUp(this.state);
  }

  validateFirstname(firstName) {
    // eslint-disable-next-line
    var re = /^[a-zA-Z0-9\s\'\-]{3,15}$/;
    return re.test(String(firstName).toLowerCase());
  }

  validateLastname(lastName) {
    // eslint-disable-next-line
    var re = /^[a-zA-Z0-9\s\'\-]{3,15}$/;
    return re.test(String(lastName).toLowerCase());
  }

  validateEmail(email) {
    // eslint-disable-next-line
    var re = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;
    return re.test(String(email).toLowerCase());
  }
  render() {
    const { auth, authError } = this.props;
    if (auth.uid) return <Redirect to='/' /> 
    return (
        <div className="container mb-5">
          <div className="row justify-content-center">
            <div className="col-12 col-md-8 col-lg-8 col-xl-6">
             <form  className="rounded"  style={{background: '#e1e1e1' }} onSubmit={this.handleSubmit} >
             <h4 style={{
                backgroundColor: 'rgba(0, 188, 212)', 
                padding: 18, 
                color: '#fff', 
                marginLeft: -18,
                marginTop: -18,
                marginRight: -18,
                borderTopRightRadius: 4,
                borderTopLeftRadius: 4
                }}>SignUp
              </h4>

              <div className="row align-items-center mt-4">
                <div className="col">
                      <label><strong>Upload Profile Pcture</strong></label> <br />
                        
                        {this.state.photoURL ? 
                        <div className="col-12 col-md-6 embed-responsive embed-responsive-16by9 mt-auto mb-auto">
                            <img className="embed-responsive-item w-100" src={this.state.photoURL} 
                            alt={"No loaded"}   /> 
                       </div> : null}
                        <br />
                        <FileUploader
                          accept="image/*"
                          name="avatar"
                          randomizeFilename
                          storageRef={firebase.storage().ref('profilePics')}
                          onUploadSuccess={this.handleUploadSuccess}
                       />
                </div> 
              </div> 
              <div className="row align-items-center mt-4">
                <div className="col">
                  <input type="email" id='email' className="form-control" placeholder="Email" onChange={this.handleChange} />
                </div>
              </div>
              <div className="row align-items-center">
                <div className="col mt-4">
                  <input type="text" id='firstName' className="form-control" placeholder="First Name" onChange={this.handleChange} />
                </div>
              </div>
              <div className="row align-items-center">
                <div className="col mt-4">
                  <input type="text" id='lastName' className="form-control" placeholder="Last Name" onChange={this.handleChange} />
                </div>
              </div>
              <div className="row align-items-center mt-4">
                <div className="col">
                  <input type="password" id='password' className="form-control" placeholder="Password" onChange={this.handleChange} />
                </div>

              </div>
              <div className="row justify-content-start mt-4">
                <div className="col">
                  <div className="form-check">
                    <Checkbox onChange={this.handleChanges}> I Read and Accept 
                       <a href="https://www.froala.com"> Terms and Conditions</a>
                    </Checkbox>
                  </div>
                  <button className="btn btn-primary mt-4">Submit</button>
                    <div className="center red-text">
                        { authError ? <p>{authError}</p> : null }
                    </div>
                </div>
              </div>
                </form>
            </div>
        </div>
        </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    auth: state.firebase.auth,
    authError: state.auth.authError
  }
}

const mapDispatchToProps = (dispatch)=> {
  return {
    signUp: (creds) => dispatch(signUp(creds))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SignUp)
