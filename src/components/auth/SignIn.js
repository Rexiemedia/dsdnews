import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Button} from 'reactstrap';
import { Link } from 'react-router-dom'
import { signIn, signInWithGoogle } from '../store/actions/authActions'
import { Redirect } from 'react-router-dom'

class Signin extends Component {
    state = {
        email: '',
        password: ''
    }
    handleChange = (e) => {
        this.setState({
            [e.target.id]: e.target.value
        })
    }
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.signIn(this.state);
        this.resetForm();
    }
    resetForm() {
        this.setState({
            email: '',
            password: ''
        });
    }

    render() {
        const { authError, auth } = this.props;
        let { lastLocation } = this.props.location.state || { lastLocation: { pathname: "/" } };
        if (auth.uid) return <Redirect to={lastLocation} />; 
  
        return ( 
            <div className="container mb-5 mt-2 project-details">
                <div className="row justify-content-center align-items-center">
                    <div className="col-12 col-md-8 col-lg-8 col-xl-6">
                        <form className="rounded"  style={{background: '#e1e1e1' }} onSubmit={this.handleSubmit} >
                            <h4 style={{
                                backgroundColor: 'rgba(0, 188, 212)', 
                                padding: 18, 
                                color: '#fff', 
                                marginLeft: -18,
                                marginTop: -18,
                                marginRight: -18,
                                borderTopRightRadius: 4,
                                borderTopLeftRadius: 4
                                }}>SignIn
                            </h4>

                            <div className="row align-items-center mt-5">
                            <div className="col">
                                <input type="email" id='email' className="form-control" placeholder="Email" onChange={this.handleChange} />
                            </div>
                            </div>

                            <div className="row align-items-center mt-4">
                            <div className="col">
                                <input type="password" id='password' className="form-control" placeholder="Password" onChange={this.handleChange} />
                            </div>

                            </div>
                            <div className="row justify-content-start mt-4">
                            <div className="col">
                            <button className="btn btn-primary mt-4">LogIn</button>
                            <div className="center red-text">
                                    { authError ? <p>{authError}</p> : null }
                            </div>
                            <p><strong className="danger mr-3">No Account?</strong> <Link to='/signup' className="btn btn-warning mr-3">SignUp</Link> <strong className="danger mr-3">OR</strong>
                            <Button style={{backgroundColor: '#2c2c2c', color: '#fff'}} outline onClick={this.props.signInWithGoogle}>
                            <i className="fab fa-google"></i> SignIn With Google </Button>
                            </p>
                            </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

    const mapStateToProps = (state) => {
        return {
            authError: state.auth.authError,
            auth: state.firebase.auth
        }
    }

    const mapDispatchToProps = (dispatch) => {
        return {
            signIn: (creds) => dispatch(signIn(creds)),
            signInWithGoogle: () => dispatch(signInWithGoogle())
        }
    }

    export default connect(mapStateToProps, mapDispatchToProps)(Signin)