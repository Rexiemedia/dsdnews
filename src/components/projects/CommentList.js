import React from 'react';
import { connect } from 'react-redux'
import { firestoreConnect } from 'react-redux-firebase'
import { Comment, Avatar } from 'antd';
import { Link,  } from 'react-router-dom'
import renderHTML from 'react-render-html'
// import moment from 'moment'

const CommentSummary = ({comment, auth}) => {
  
  return (
      <Comment
      className="ml-1 mr-1"
          // actions={[<span>Reply to</span>]}
            // eslint-disable-next-line
          author={<span>{comment.authorFirstName}</span>}
          avatar={(
            <Avatar
              src={comment.photoURL}
              alt={comment.authorFirstName}
            />
          )}
          content={renderHTML(comment.value)}
        >
          {/* {children} */}
      </Comment>
  )
}

const CommentList = (props) => {
  const { comments, auth, articleId } = props;

  return(
    <React.Fragment>
          { comments && comments.filter((comments) => comments.articleId === articleId).map(comment => {
            return (
              <Link to={'/project/' + comment.id} key={comment.id}>
                  <CommentSummary comment={comment} auth={auth} />
              </Link>
              )
          })}  
    </React.Fragment>           
  )
}
const mapStateToProps = (state) => {
  // console.log(state);
  return {
    comments: state.firestore.ordered.comments,
    auth: state.firebase.auth,
  }
}

// export default HomeComponent;
export default firestoreConnect([{ collection: 'comments', orderBy: ['createdAt', 'desc'] }])( connect( mapStateToProps )(CommentList) )
