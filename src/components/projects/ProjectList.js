import React from 'react'
import PorjectSummary from './projectSummary'
import { Link } from 'react-router-dom'

const ProjectList = ({projects, auth}) => {
        return(
            <div className="project-list section">
            { projects && projects.map(project => {
              return (
                <Link to={'/project/' + project.id} key={project.id}>
                  <PorjectSummary project={project} auth={auth} />
                </Link>
              )
            })}  
          </div>
        )
}

export default ProjectList