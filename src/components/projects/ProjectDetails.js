import React, { Component } from 'react';
import { connect } from 'react-redux'
import { firestoreConnect } from 'react-redux-firebase'
import { compose } from 'redux'
import moment from 'moment'
import Loading from '../LoadingComponent'
import renderHTML from 'react-render-html'
import {Helmet} from 'react-helmet'
import CreateCmment from './CreateCmment'
import {
  FacebookShareButton,
  GooglePlusShareButton,
  LinkedinShareButton,
  TwitterShareButton,
  WhatsappShareButton,
} from 'react-share';
import {
  FacebookIcon,
  TwitterIcon,
  WhatsappIcon,
  GooglePlusIcon,
  LinkedinIcon,
} from 'react-share';

class ProjectDetails extends Component {
 
  
  render() { 
    let { project, auth,  user } = this.props
    const shareUrl = window.location.href; 
    const articleId = this.props.match.params.id; 
    const {pathname} = this.props.location;
            
    if (project) {

      return (
        <div className="container section project-details mt-3 mb-4 ">
          <Helmet>
            <meta charSet="utf-8" />
            <title>{project.title}</title>
            <meta name="description" content={project.content}  itemProp="description"
            />
            <meta name="google-site-verification" content="5etYwSJdPvs73RVzF_Hb-YPow1mvMGynMVfCWgoLQuo" />
            <meta property="og:url" content={shareUrl} />
            <meta property="og:site_name" content="DSD News" />
            <meta property="og:image" content={project.pictureUrl} itemProp="image" />
            <meta property="og:image:url" content={project.pictureUrl} itemProp="image" />
            <meta property="og:image:width" content="700" />
            <meta property="og:image:height" content="400" />
            <meta property="og:title" content={project.title} itemProp="name" />
            <meta property="og:type" content="website" />
            <meta property="og:description" content={project.title} />
            <meta property="fb:app_id" content="198985484382564" />
          </Helmet>
          <div className="card z-depth-0 col-12 col-sm-8 mx-auto">
          {project.pictureUrl ?             
            <div className="embed-responsive embed-responsive-16by9 mt-3 mb-0">
              <img src={project.pictureUrl} className="embed-responsive-item w-100" alt="NoPics" />
            </div> : null}
            <div className="card-content mt-2">
              <span className="card-title h4 ">{project.title}</span> <br /><br />
              {renderHTML(project.content)}
            </div>
            <div className="card-action grey lighten-2 mb-3">
              <div>Posted by {project.authorFirstName} {project.authorLastName}</div>
              <div>{moment(project.createdAt.toDate()).calendar()}</div>
            </div>
           
            <div style={{display: 'inherit'}} className="mx-auto" >
           
           <FacebookShareButton
             url={shareUrl}
             quote={`${project.title}`}
             className="Demo__some-network__share-button m-2 ml-0">
             <FacebookIcon size={32} round={true} />
           </FacebookShareButton>

           <GooglePlusShareButton
             url={shareUrl}
             title={project.title}
             media={project.pictureUrl}
             className="Demo__some-network__share-button m-2">
             <GooglePlusIcon size={32} round={true} />
           </GooglePlusShareButton>
           
           <TwitterShareButton
             url={shareUrl}
             title={project.title}
             media={project.pictureUrl}
             className="Demo__some-network__share-button m-2">
             <TwitterIcon size={32} round={true} />
           </TwitterShareButton>

           <WhatsappShareButton
             url={shareUrl}
             title= {project.title}
             pictureUrl={project.pictureUrl}
             className="Demo__some-network__share-button m-2">
             <WhatsappIcon size={32} round={true} />
           </WhatsappShareButton>

           <LinkedinShareButton
             url={shareUrl}
             title={project.title}
             description={project.pictureUrl}
             className="Demo__some-network__share-button m-2">
             <LinkedinIcon size={32} round={true} />
           </LinkedinShareButton>
           </div> 
            <CreateCmment articleId={  articleId } auth={auth} pathname={pathname} user={user} />                 
          </div>
        </div>
      )
    } 
    else {
      return(
        <div className="container section project-details mt-5 pt-5">
        <div className="card z-depth-0 col-12 col-md-8 mx-auto">
          <div className="spinner mx-auto pt-2">
            <Loading />
          </div>
        </div>
      </div>
      )
    }
  }
}

const mapStateToProps = (state, ownProps) => {
  // console.log(state);
  const id = ownProps.match.params.id;
  const uid = state.firebase.auth.uid;
  const projects = state.firestore.data.projects;
  const users = state.firestore.data.users;
  const user = users ? users[uid] : null;
  const project = projects ? projects[id] : null;

  return {
    project: project,
    auth: state.firebase.auth,
    user: user
  }
}
export default compose(
  connect(mapStateToProps),
  firestoreConnect([
    { collection: 'projects' },
    { collection: 'users' }
  ])
)(ProjectDetails)

// export default firestoreConnect([{ collection: "projects" }])( connect( mapStateToProps )(ProjectDetails) )