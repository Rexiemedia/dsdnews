import React, { Component } from 'react';
import { connect } from 'react-redux'
import { createComment } from '../store/actions/commentAction'
import CommentList from './CommentList'
import {withRouter} from 'react-router-dom';
import ReactQuill from 'react-quill'
import 'react-quill/dist/quill.snow.css'
  
 
  class CreateCmment extends Component {
    
    state = {
      articleId: '',
      photoURL: '',
      value: ''
    }

    handleSubmit = (e) => {
      e.preventDefault();
     
      if (!this.state.value) {
        return;
      }
      const { auth } = this.props;

      if (!auth.uid) {
        const {pathname} = this.props;
        // console.log(pathname);
        let lastLocation = pathname;
        this.props.history.push('/signin', lastLocation={lastLocation});
    }

    else{
      this.props.CreateCmment(this.state);
      this.setState({
        value: '',
        photoURL: '',
        articleId: ''
      });
    } 
  }

onHandleChange = (e) => {
  this.setState({ 
          value: e
   }); 
   const {articleId } = this.props;
   const {photoURL} = this.props.user;
   
   this.setState({
    articleId: articleId,
    photoURL: photoURL
});

} 
    render() {
      const { value } = this.state;
      const articleId = this.props.articleId;
        
      return (
        <React.Fragment>
          <hr />
          <ReactQuill
            className="userForm"
            placeholder="Enter Comment"
            onChange={this.onHandleChange}
            defaultValue={this.value || ''}
            value={value}
          />
          <br />
          <button className="btn btn-primary" onClick={this.handleSubmit}>Submit</button>
          < hr />
          <CommentList articleId={articleId} />
        </React.Fragment>
      );
    }
  }
  const mapStateToProps = (state) => {
    return {
      auth: state.firebase.auth
    }
  }
  
  const mapDispatchToProps = dispatch => {
    return {
      CreateCmment: (comment) => dispatch(createComment(comment))
    }
}
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(CreateCmment)) 
