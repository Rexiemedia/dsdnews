import React, { Component } from 'react'
import { connect } from 'react-redux'
import { updateProject } from '../store/actions/projectActions'
import { Redirect } from 'react-router-dom'

class UpdateProject extends Component {
    state = {
        title: '',
        content: ''
    }
    handleChange = (e) => {
    this.setState({
        [e.target.id]: e.target.value
    })
    }
    handleSubmit = (e) => {
    e.preventDefault();
    // console.log(this.state);
    this.props.updateProject(this.state);
    this.props.history.push('/');
    }

    render() { 
        // eslint-disable-next-line
        const { project, auth } = this.props;
        if (!auth.uid === 'OPMwH4QrCcc0oWMPh8PukDOz1OI3' || auth.uid === undefined) return <Redirect to='/signin' /> 
        return ( 
            <div className="container mt-2">
                <div className="row">
                    <div className="col-12 col-md-8 justify-content-center">
                        <form className="white" onSubmit={this.handleSubmit}>
                            <h5 className="grey-text text-darken-3">Create a New Project</h5>
                            <div className="input-field">
                                <input type="text" id='title' onChange={this.handleChange} />
                                <label htmlFor="title">Project Title</label>
                            </div>
                            <div className="input-field">
                                <textarea id="content" className="materialize-textarea" onChange={this.handleChange}></textarea>
                                <label htmlFor="content">Project Content</label>
                            </div>
                            <div className="input-field">
                                <button className="btn pink lighten-1">Create</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
         );
    }
}
 
const mapStateToProps = (state) => {
    return {
      auth: state.firebase.auth
    }
  }
  
  const mapDispatchToProps = dispatch => {
    return {
      updateProject: (project) => dispatch(updateProject(project))
    }
  }
  
  export default connect(mapStateToProps, mapDispatchToProps)(UpdateProject)