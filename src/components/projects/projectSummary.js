import React from 'react'
import { Link } from 'react-router-dom'
import moment from 'moment'

const PorjectSummary = ({project, auth}) => {

    if ( auth.uid === '3rWOMvlI2vZqfuNCvPaWKe4CFi03') {
        return (
            <div className="row card mb-3">
                <div className="col-10 m-1 pt-2 pb-2">
                    <div className="card-content text-left">
                        <span className="card-title">{project.title}</span>
                        <p>Posted by {project.authorFirstName} {project.authorLastName}</p>
                        <p className="grey-text">{moment(project.createdAt.toDate()).calendar()}
                        <span className="ml-2">
                            <Link to={'/updateproject/' + project.id} className="btn btn-warning mr-1">Update</Link>
                            <Link to={'/delete/' + project.id} className="btn btn-danger">Delete</Link>
                        </span>
                        </p>
                    </div>
                </div>
            </div>
        )
    }
    return(
        <div className="container justify-content-center">
         <div className="row">
            <div className="card col-12 mb-1">
                <div className="card-content mt-auto text-left">
                    <span className="card-title h5 mb-0 pb-0 text-success">{project.title}</span>
                    <p>Posted by {project.authorFirstName} {project.authorLastName} <br />
                    <span>{moment(project.createdAt.toDate()).calendar()}</span>
                    </p>
                </div>
            </div>
        </div> 
        </div>
    )     
}
export default PorjectSummary;