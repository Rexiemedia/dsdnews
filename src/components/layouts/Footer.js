import React, {Component} from 'react'
import { Link } from 'react-router-dom'
import { Button, Modal, ModalHeader, ModalBody } from 'reactstrap';


class Footer extends Component {

  state = {
    isModalOpen: false
}


toggleModal = () => {
this.setState({
    isModalOpen: !this.state.isModalOpen
});
}
 render(){
  return (
    <footer className="site-footer bg-light alert-info" role="alert">
    <div className="container pt-4 pb-2">

      <ul className="nav justify-content-center mb-2">
        <Button style={styles.buttons} onClick={this.toggleModal} className="nav-item nav-link privacy">Privacy policy</Button>
        <Link  style={styles.buttons} to='/contactus' className="nav-item nav-link">Share your Story</Link>
        <Link style={styles.buttons} to='/contactus' className="nav-item nav-link">Advertise</Link>
        <Link style={styles.buttons} to='/contactus' className="nav-item nav-link">Contact Us</Link>
      </ul>
      <div className="d-flex justify-content-center mb-3">
        © DSD News 2018 All rights reserved
      </div>
     </div>
     <Modal isOpen={this.state.isModalOpen} toggle={this.toggleModal}>
      <ModalHeader toggle={this.toggleModal}>Privacy Policy</ModalHeader>
        <ModalBody>
          <div className="col">
            <pre>
            TERMS AND PRIVACY POLICY

                Dsdnews is passionately committed to inform and update you the happenings within and outside your environment. Dsdnews as a media network on behalf of itself, affiliates and partners, wish to inform individuals outside the organization that we interact with, our site visitors and users of our service, that our terms and privacy policy entails the use of information that we obtain through the use and connections to our site.

                Using our web site connotes that you have read and accept dsdnews privacy policy and practice. But contrary to our terms and privacy policy implies you do not agree. And because you have your right to disagree, we cannot deprive you of your fundamental rights, so do not access or use our web site.

                Dsdnews.de is a registered media network with Godaddy.com with http||www.dsdnews.com in a nutshell you can use and visit www.dsdnews.de without telling us or indentifying any of your personal information.

                                  DATA AND INFORMATION USAGE

                It is assured that we may collect your personal data from any of our social media platform from where you contact us and use our services. Amendment and updating may also occur from time to time which will eventually result to changes in our usage of your personal data process or changes legally by law if applicable.                           

                So we admonish you to read and review the information carefully.

                                  

                DATA SECURITY

                We also wish to inform you that your personal data that you will contact us with will be totally protected according or pursuant to data protection laws specially in EU. General data protection regulation include

                1.    You have right to be inform about our collection and use of your personal data.

                2.    The right to object us from using your personal data.

                CONTACT

                For more information concerning our terms, privacy and policy, you can contact us at kelvinlucky.edion1987@gmail.com and also at www.dsdnews.de
            </pre>
          </div>
        </ModalBody>
     </Modal>
  </footer>
  )
 }
 
}
const styles ={
  buttons: {
    backgroundColor: 'transparent', color: '#1890ff', border: 'none'
  }
}
export default Footer