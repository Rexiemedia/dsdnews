import React, { Component } from 'react';
import { Navbar, NavbarBrand, Nav, NavbarToggler, Collapse, NavItem } from 'reactstrap';
import { NavLink } from 'react-router-dom'
import SignedInLink from './SignedInLink'
import SignedOutLinks from './SignedOutLink'
import { connect } from 'react-redux'

class NavBar extends Component{
  constructor(props) {
        super(props);

        this.state = {
            isNavOpen: false,
        };

        this.onBlur = this.onBlur.bind(this);
      }

      toggleNav = () => {
        this.setState({
          isNavOpen: !this.state.isNavOpen
        });
      }

      onBlur = (e) => {
       var currentTarget = e.currentTarget;
       setTimeout(function() {
         if (!currentTarget.contains(document.activeElement)) {
           this.setState({
             isNavOpen: !this.state.isNavOpen
           });
         }
       }.bind(this), 0.50);
   }

  render(){
  const { auth, profile } = this.props;
  const links = auth.uid ? <SignedInLink profile={profile} /> : <SignedOutLinks />;

  return (
          // Declaring a fraction <> like this prevent extra node on the DOM
      <React.Fragment>
        <Navbar dark className="bg-primary" expand="md">
          <div className="container">
              <NavbarToggler onClick={this.toggleNav} onBlur={this.onBlur} />
              <NavbarBrand className="mr-auto" href="/"><img src='assets/images/logo.png' height="30" width="41" alt='DSD News' /></NavbarBrand>
              <span className="space-bar" />
              <Collapse isOpen={this.state.isNavOpen} navbar>
                  <Nav navbar>
                  <NavItem>
                      <NavLink className="nav-link"  to='/'><span className="fa fa-home"></span> Home <span className="sr-only">Home</span></NavLink>
                  </NavItem>
                  <NavItem>
                      <NavLink className="nav-link"  to='/politics'><span className="fas fa-vote-yea"></span> Politics </NavLink> 
                  </NavItem>
                  <NavItem>
                      <NavLink className="nav-link"  to='/crime-watch'><span className="fas fa-user-ninja"></span> Crime </NavLink>
                  </NavItem>
                  <NavItem>
                      <NavLink className="nav-link"  to='/sports'><span className="fas fa-running"></span> Sports </NavLink>
                  </NavItem>
                  <NavItem>
                      <NavLink className="nav-link" to='/entertainment'><span className="fab fa-creative-commons-sampling"></span> Entertainment </NavLink>
                  </NavItem>
                  </Nav>
                  <Nav className="ml-auto" navbar>
                    { links }  
                  </Nav>
              </Collapse>
            </div>
        </Navbar>
    </React.Fragment>
    )
  }
}
const mapStateToProps = (state) => {
  // console.log(state);
  return{
    auth: state.firebase.auth,
    profile: state.firebase.profile
  }
}
export default connect(mapStateToProps)(NavBar)
