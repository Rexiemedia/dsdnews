import React from 'react';
import { Breadcrumb, BreadcrumbItem } from 'reactstrap';

const Crumbs = (props) => {
  return (
    <div className="col-12">
      <Breadcrumb tag="nav">
        <BreadcrumbItem tag="a" href="#">DashBoard</BreadcrumbItem>
        <BreadcrumbItem tag="a" href="#">Library</BreadcrumbItem>
        <BreadcrumbItem tag="a" href="#">Data</BreadcrumbItem>
        <BreadcrumbItem active tag="span">Bootstrap</BreadcrumbItem>
      </Breadcrumb>
    </div>
  );
};

export default Crumbs;