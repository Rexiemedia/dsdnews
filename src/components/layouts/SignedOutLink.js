import React from 'react'
import { NavItem} from 'reactstrap';
import { NavLink } from 'react-router-dom'


const SignedOutLinks = (props) => {
    return (
      <React.Fragment>
        <NavItem>
          <NavLink to='/signin' className="nav-link">LogIn</NavLink>
        </NavItem>
    </React.Fragment>
  )
}

export default SignedOutLinks
