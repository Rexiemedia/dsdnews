import React from 'react'
import { NavItem, Button} from 'reactstrap';
import { NavLink } from 'react-router-dom'
import { connect } from 'react-redux'
import { signOut } from '../store/actions/authActions'


const SignedInLink = (props) => {
  const { profile, auth } = props;
  const initials = profile.initials ? <NavLink to='/' className="nav-link btn btn-primary rounded-circle">
  {props.profile.initials}</NavLink> : null;

const dashboard = auth.uid === '3rWOMvlI2vZqfuNCvPaWKe4CFi03' ? <NavItem><NavLink to='/dashboard' className="nav-link">Dash Board</NavLink></NavItem> : null ;
    return (
      <React.Fragment>
          {dashboard}
          <NavItem className="mr-1">
              <Button style={{color: '#fff'}} outline onClick={props.signOut}><span className="fa fa-sign-out"></span> LogOut</Button>
          </NavItem>
          <NavItem>
            {initials}
          </NavItem>
      </React.Fragment>
     )
}

const mapStateToProps = (state) => {
  return{
    auth: state.firebase.auth,
    profile: state.firebase.profile
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    signOut: () => dispatch(signOut())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SignedInLink)

