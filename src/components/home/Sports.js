import React, { Component } from 'react'
import ProjectList from '../projects/ProjectList'
import { connect } from 'react-redux'
import { firestoreConnect } from 'react-redux-firebase'
import Loading from '../LoadingComponent'
import {Helmet} from 'react-helmet'
import Notification from '../dashboard/Notifications'


class SportsComponent extends Component {
    
    render() { 
        const { projects, auth, notifications } = this.props;
         
        if(projects){
            return ( 
                <div className="container mt-2 project-details">
                    <div className="row">
                        <div className="col-12 col-md-8">
                            <ProjectList projects={this.props.projects.filter((project) => project.sports)} auth={auth} />
                        </div>
                        <div className="col-12 col-md-4 mt-2 justify-content-center">
                            <Notification notifications={notifications} auth={auth} />
                        </div>
                    </div>
                    <Helmet>
                        <meta charSet="utf-8" />
                        <title>DSD Current Sports News and highlights</title>
                        <meta name="description" content="Welcome to DSD current News Page" />
                    </Helmet>
                </div>
             );
        }
        return(
            <div className="container section project-details mt-5 pt-5">
            <div className="card z-depth-0 col-12 col-md-8 mx-auto">
              <div className="spinner mx-auto pt-2">
                <Loading />
              </div>
            </div>
          </div>
        )
    }
}

const mapStateToProps = (state) => {
    // console.log(state);
    return {
      projects: state.firestore.ordered.projects,
      auth: state.firebase.auth,
      notifications: state.firestore.ordered.notifications,
    }
}

// export default HomeComponent;
export default firestoreConnect([
    { collection: 'projects',  limit: 10, orderBy: ['createdAt', 'desc']},
    { collection: 'notifications', limit: 10, orderBy: ['time', 'desc']}
])( connect( mapStateToProps )(SportsComponent) )