import React, { Component } from 'react'
import ProjectList from '../projects/ProjectList'
import { connect } from 'react-redux'
import { firestoreConnect } from 'react-redux-firebase'
import Loading from '../LoadingComponent'
import Notification from '../dashboard/Notifications'
import {Helmet} from 'react-helmet'


class CrimeComponent extends Component {
    
    render() { 
        const { projects, auth, notifications } = this.props;
 
        if(projects){
            return ( 
                <div className="container mt-2 project-details">
                    <Helmet>
                        <meta charSet="utf-8" />
                        <title>DSD Current news on Crime watch around the world</title>
                        <meta name="description" content="Welcome to DSD current News Page" />
                    </Helmet>
                    <div className="row">
                        <div className="col-12 col-md-8">
                            <ProjectList projects={this.props.projects.filter((project) => project.crimeWatch)} auth={auth} />
                        </div>
                        <div className="col-12 col-md-4 mt-2 justify-content-center">
                            <Notification notifications={notifications} auth={auth} />
                        </div>
                    </div>
                </div>
             );
        }
        return(
            <div className="container section project-details mt-5 pt-5">
            <div className="card z-depth-0 col-12 col-md-8 mx-auto">
              <div className="spinner mx-auto pt-2">
                <Loading />
              </div>
            </div>
          </div>
        )
    }
}

const mapStateToProps = (state) => {
    // console.log(state);
    return {
      projects: state.firestore.ordered.projects,
      auth: state.firebase.auth,
      notifications: state.firestore.ordered.notifications
    }
}

// export default HomeComponent;
export default firestoreConnect([
    { collection: 'projects', orderBy: ['createdAt', 'desc'], limit: 10},
    { collection: 'notifications', orderBy: ['time', 'desc'], limit: 10 }
])( connect( mapStateToProps )(CrimeComponent) )