import React, { Component } from 'react'
import Notification from '../dashboard/Notifications'
import ProjectList from '../projects/ProjectList'
import { connect } from 'react-redux'
import { firestoreConnect } from 'react-redux-firebase'
import Loading from '../LoadingComponent'
import {Helmet} from 'react-helmet'

class LatestComponent extends Component {
  
// article={this.props.articles.articles.filter((article) => article.featured)[0]}
    render() { 
        // eslint-disable-next-line  
        const { projects, auth, notifications } = this.props;
        if(projects){
            return ( 
                <div className="container mt-2 project-details">
                    <Helmet>
                        <meta charSet="utf-8" />
                        <title>
                            Welcome to DSD News: current news on Politics, Crime watch, Sports highlights, 
                            entertainment in the music industry, current affairs in the middle east, European parliaments 
                            and Germany Politics/ Institutional racism, Africa dictators & politics, Islamic terrorism, 
                            pre-colonia tax, coco bean child slavery, influence of the west in Africa politics 
                            and more ... 
                        </title>
                    </Helmet>
                    <div className="row">
                        <div className="col-12 col-md-8 pt-2">
                            <ProjectList projects={projects} auth={auth} />
                        </div>
                        <div className="col-12 col-md-4 mt-2 justify-content-center">
                            <Notification notifications={notifications} auth={auth} />
                        </div>
                    </div>
                </div>
             );
        }
        return(
            <div className="container section project-details mt-5 pt-5">
            <div className="card z-depth-0 col-12 col-md-8 mx-auto">
              <div className="spinner mx-auto pt-2">
                <Loading />
              </div>
            </div>
          </div>
        )
    }
}

const mapStateToProps = (state) => {
    // console.log(state);
    return {
      projects: state.firestore.ordered.projects,
      auth: state.firebase.auth,
      notifications: state.firestore.ordered.notifications
    }
}

// export default HomeComponent;
export default firestoreConnect([
    { collection: 'projects', orderBy: ['createdAt', 'desc'], limit: 50},
    { collection: 'notifications', orderBy: ['time', 'desc'], limit:50  }
])( connect( mapStateToProps )(LatestComponent) )