import React, { Component } from 'react'
import Notification from '../dashboard/Notifications'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { firestoreConnect } from 'react-redux-firebase'
import Loading from '../LoadingComponent'
import {Helmet} from 'react-helmet'
import CaroulseltList from '../homecomp/Carousellist'
import Categry from '../homecomp/Categories'



class HomeComponent extends Component {
  
    render() { 
        const { projects, auth, notifications, loading } = this.props;
        if(loading){
            return(
                <div className="container section project-details mt-5 pt-5">
                <div className="card z-depth-0 col-12 col-md-8 mx-auto">
                  <div className="spinner mx-auto pt-2">
                    <Loading />
                  </div>
                </div>
              </div>
            )
        }
       else if(projects){
            return ( 
                <div className="container mt-2 project-details">
                    <Helmet>
                        <meta charSet="utf-8" />
                        <title>
                            Welcome to DSD News HomePage with current news on Politics, Crime watch, Sports highlights, 
                            entertainment in the music industry, current affairs in the middle east, European parliaments 
                            and Germany Politics/ Institutional racism, Africa dictators politics, Islamic terrorism, 
                            pre-colonia tax, coco beans child slavery, influence of the west in Africa politics 
                            and more ... 
                        </title>
                        <meta name="description" content="Welcome to DSD News Home Page" />
                    </Helmet>

                    <div className="row">
                        <div className="col-12 col-md-8">
                           
                            <div className="col-12 carousel-top pt-2 rounded justify-content-center">
                                 <CaroulseltList />
                            </div>
                            <div className="col-12 pt-2 rounded justify-content-center">
                                 <Categry />
                            </div>
                        </div>
                        <div className="col-12 col-md-4 mt-2 mb-2 justify-content-center">
                            <Notification notifications={notifications} auth={auth} />
                        </div>
                    </div>
                </div>
             );
        }
        else if(!projects || !loading) {
            return(null)
        }

    }
}

const mapStateToProps = (state) => {
    // console.log(state);
    return {
      projects: state.firestore.ordered.projects,
      auth: state.firebase.auth,
      notifications: state.firestore.ordered.notifications
    }
  }
  
  export default compose(
    connect(mapStateToProps),
    firestoreConnect([
      { collection: 'projects', orderBy: ['createdAt', 'desc']},
      { collection: 'notifications', limit: 20, orderBy: ['time', 'desc']}
    ])
  )(HomeComponent)