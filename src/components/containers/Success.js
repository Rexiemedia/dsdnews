import React, { Component } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import RaisedButton from 'material-ui/RaisedButton';
import {withRouter} from 'react-router-dom';


export class Success extends Component {
  continue = e => {
    e.preventDefault();
    // PROCESS FORM //
    this.props.history.push('/');
  };

  back = e => {
    e.preventDefault();
    this.props.prevStep();
  };

  render() {
    
    return (
      <MuiThemeProvider>
        <React.Fragment>
          {/* <AppBar title="Successfully Sent" /> */}
          <h3>Thank You For Your Submission</h3>
          <p className="mb-2 text-center">We shall contact you shortly.</p>
          <br />
          <RaisedButton
            label="Continue"
            primary={true}
            style={styles.button}
            onClick={this.continue}
          />
        </React.Fragment>
      </MuiThemeProvider>
    );
  }
}


const styles = {
  button: {
    margin: 5,
    marginBottom: 10
  }
};

export default withRouter(Success);
