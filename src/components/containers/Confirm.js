import React, { Component } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
// import AppBar from 'material-ui/AppBar';
import { List, ListItem } from 'material-ui/List';
import RaisedButton from 'material-ui/RaisedButton';
import { connect } from 'react-redux'
import { contactUs } from '../store/actions/contactAction'
import renderHTML from 'react-render-html'


export class Confirm extends Component {
  continue = e => {
    // e.preventDefault();
    
    // PROCESS FORM //
    const {values} = this.props
    this.onHandleSubmit(values);
    this.props.nextStep();
  };

  back = e => {
    e.preventDefault();
    this.props.prevStep();
  };
  onHandleSubmit = ( values) => {
    this.props.contactUs(values);
  }

  render() {
    const {
      values: { firstName, lastName, email, telnum, content }
    } = this.props;
   
    return (
      <MuiThemeProvider>
        <React.Fragment>
          {/* <AppBar title="Confirm User Data" /> */}
          <List>
            <ListItem primaryText="First Name" secondaryText={firstName} />
            <ListItem primaryText="Last Name" secondaryText={lastName} />
            <ListItem primaryText="Email" secondaryText={email} />
            <ListItem primaryText="Tel:" secondaryText={telnum} />
            <ListItem primaryText="Message" secondaryText={renderHTML(content)} />
          </List>
          <br />
          <RaisedButton
            label="Confirm & Continue"
            primary={true}
            style={styles.button}
            onClick={this.continue}
          />
          <RaisedButton
            label="Back"
            primary={false}
            style={styles.button}
            onClick={this.back}
          />
        </React.Fragment>
      </MuiThemeProvider>
    );
  }
}

const styles = {
  button: {
    margin: 5,
    marginBottom: 10
  }
};

const mapDispatchToProps = dispatch => {
  return {
    contactUs: (contact) => dispatch(contactUs(contact))
  }
}

export default connect(null, mapDispatchToProps)(Confirm)
