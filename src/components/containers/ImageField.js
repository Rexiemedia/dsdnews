import React, { Component } from 'react';
import PreviewPicture from './PrevewPicture'

class InputField extends Component {
  displayPicture(event) {
    let reader = new FileReader();
    let file = event.target.files[0];
    reader.onloadend = () => {
      this.setState({
        picture: file,
        pictureUrl: reader.result
      });
      console.log(this.state.pictureUrl);
    };
    reader.readAsDataURL(file);
  }

  constructor(state) {
    super(state);
    this.state = {
      avatar: 'Please attach a picture',
      pictureUrl: ''
    };
  }

  render() {
    const { required, input, label } = this.props;
    // delete input.value;
    return (
      <div>
        <div className="form-group row">
          <label className="col-sm-3 col-form-label">{`${label} ${required ? '*' : ''}`}</label>
          <div className="col-sm-9">
            <input type="file"
                   className="form-control"
                   {...input}
                   onChange={(event) => {
                     this.displayPicture(event);
                   }}
            />
          </div>
        </div>
        <PreviewPicture picture={this.state.avatar} pictureUrl={this.state.pictureUrl}/>
      </div>
    );
  }
}

export default InputField;
