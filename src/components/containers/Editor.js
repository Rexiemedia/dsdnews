import React from 'react';
import ReactQuill from 'react-quill'
import 'react-quill/dist/quill.snow.css'
import AppBar from 'material-ui/AppBar';

const FormEditor = (props) => {

    const { values, handleChange } = props;

    return (
        <React.Fragment>
            <AppBar title="must be dynamic" />
            <ReactQuill
              className="userForm"
              placeholder="Enter Content"
              onChange={handleChange('content')}
              defaultValue={values.content}
            />
        </React.Fragment>
    );
  }

export default FormEditor;
