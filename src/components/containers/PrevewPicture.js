import React from 'react';

const PreviewPicture = (props) => {
  const { avatar, pictureUrl } = props;

  if (!pictureUrl) {
    return (
      <div style={{
        height: '300px', borderWidth: '.1rem',
        borderStyle: 'solid', borderColor: 'grey'
      }} className="text-center mb-1">
        {avatar}
      </div>
    );
  }
  else {
    return (
      <img className="img-fluid mb-2 mt-2" src={pictureUrl} alt="Upload" />
    );
  }
};

export default PreviewPicture;