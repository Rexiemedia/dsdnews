import React, { Component, Fragment } from 'react';
import FormUserDetails from './FormUserDetails';
import FormPersonalDetails from './FormPersonalDetails';
import Confirm from './Confirm';
import Success from './Success';
import ReactQuill from 'react-quill'
import 'react-quill/dist/quill.snow.css'
import { Redirect } from 'react-router-dom'
import RaisedButton from 'material-ui/RaisedButton';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

export class UserForm extends Component {
  state = {
    step: 1,
    firstName: '',
    lastName: '',
    email: '',
    telnum: '',
    content: ''
  };

  // Proceed to next step
  nextStep = () => {
    const { step } = this.state;
  
    this.setState({
      step: step + 1
    });
  };

  // Go back to prev step
  prevStep = () => {
    const { step } = this.state;
    this.setState({
      step: step - 1
    });
  };

  // Handle fields change
  handleChange = input => e => {
    this.setState({ [input]: e.target.value });
  };
  onHandleChange = (e) => {
    this.setState({ content: e });  
  }

  onHandleSubmit = (e) => {
    e.preventDefault();
  
    this.props.contactUs(this.state);
    this.props.history.push('/');

    this.setState({
      step: 1,
      firstName: '',
      lastName: '',
      email: '',
      telnum: '',
      content: ''
    });
  }

 validateTelnum = () => {
  const { telnum, content } = this.state;
  if(!this.isNumber(telnum)){
    return;
  }
   else if(!this.validateContent(content)){
    return;
  }
    this.nextStep();
 }

 isNumber = (telnum) => {
    var re = /^[0-9]{10,12}$/;
    return re.test(String(telnum).toLowerCase());
}

 validateContent = (content) => {
    var re = /^.{10,500}$/;
    return re.test(String(content).toLowerCase());
 }
  redirectToHome = () => {
    return <Redirect to='/' />;
  }
  render() {
    const { step } = this.state;
    const { firstName, lastName, email, telnum, content } = this.state;
    const values = { firstName, lastName, email, telnum, content };

    switch (step) {
      case 1:
        return (
          <FormUserDetails
            nextStep={this.nextStep}
            handleChange={this.handleChange}
            values={values}
          />
        );
      case 2:
        return (
          <MuiThemeProvider>
             <Fragment>
              <FormPersonalDetails
                nextStep={this.nextStep}
                prevStep={this.prevStep}
                handleChange={this.handleChange}
                values={values}
              />
              <br />
              <ReactQuill
                className="userForm"
                placeholder="Enter Message"
                onChange={this.onHandleChange}
                defaultValue={values.content || ''}
                values={values}
              />
              <br />
              <RaisedButton
                label="Back"
                primary={false}
                style={styles.button}
                onClick={this.prevStep}
              />
              <RaisedButton
                label="Continue"
                primary={true}
                style={styles.button}
                onClick={this.validateTelnum}
              />
            </Fragment>
          </MuiThemeProvider>
        );
        case 3:
        return (
          <Confirm
            nextStep={this.nextStep}
            prevStep={this.prevStep}
            values={values}
          />
        );
      case 4:
      // console.log(this.state);
        return <Success values={values} redirectToHome={this.redirectToHome} />;
      default:
          return <Redirect to='/' />;
    }
  }
}

const styles = {
  button: {
    margin: 5,
    marginBottom: 10
  }
};
export default UserForm
