import React, { Component } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
// import AppBar from 'material-ui/AppBar';


export class FormUserDetails extends Component {
  continue = e => {
    e.preventDefault();
    const {values} = this.props;
    const email = values.email;
    const firstName = values.firstName;
    const lastName = values.lastName;  

    
    if(!this.validateFirstname(firstName) || !this.validateLastname(lastName)){
      return;
    }
    else if(!this.validateEmail(email)){
      return;
    };
         
    this.props.nextStep();
  };

  validateFirstname(firstName) {
    // eslint-disable-next-line
    var re = /^[a-zA-Z0-9\s\'\-]{3,15}$/;
    return re.test(String(firstName).toLowerCase());
  }

  validateLastname(lastName) {
    // eslint-disable-next-line
    var re = /^[a-zA-Z0-9\s\'\-]{3,15}$/;
    return re.test(String(lastName).toLowerCase());
  }

  validateEmail(email) {
    // eslint-disable-next-line
    var re = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;
    return re.test(String(email).toLowerCase());
  }

  render() {

    const { values, handleChange } = this.props;
    
    return (
      <MuiThemeProvider>
        <React.Fragment>
          {/* <AppBar title="Enter your details" /> */}
          <TextField
            style={styles.input}
            hintText="Enter Your First Name"
            floatingLabelText="First Name"
            onChange={handleChange('firstName')}
            defaultValue={values.firstName}
          />
          <div
            className="text-danger"
            model=".firstname"
            show="touched"
              messages={{
                  required: 'Required ',
                  minLength: 'Must be greater than 2 characters. ',
                  maxLength: 'Must be 15 characters or less. ', // eslint-disable-next-line
                  validFn: 'Not allowed: < > `` % \ /'
              }}
          />
          <br />
          <TextField
            style={styles.input}
            hintText="Enter Your Last Name"
            floatingLabelText="Last Name"
            onChange={handleChange('lastName')}
            defaultValue={values.lastName}
          />
          <br />
          <TextField
            style={styles.input}
            hintText="Enter Your Email"
            floatingLabelText="Email"
            onChange={handleChange('email')}
            defaultValue={values.email}
            type="email"
          />
          <br />
          <RaisedButton
            label="Continue"
            primary={true}
            style={styles.button}
            onClick={this.continue}
          />
        </React.Fragment>
      </MuiThemeProvider>
    );
  }
}

const styles = {
  button: {
    margin: 5,
    marginBottom: 10
  },
  input: {
    width: '95%', 
    marginLeft: 10, 
    marginRight: 10
  }
};

export default FormUserDetails;
