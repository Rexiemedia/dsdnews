import React, { Component } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import TextField from 'material-ui/TextField';
// import RaisedButton from 'material-ui/RaisedButton';

export class FormPersonalDetails extends Component {
  continue = e => {
    e.preventDefault();
    this.props.nextStep();
  };

  back = e => {
    e.preventDefault();
    this.props.prevStep();
  };

  validateTelnum(telnum){
    var re = /^[0-9][1-9]{10,12}$/;
    return re.matches(String(telnum).toLowerCase());
  }

  render() {
    const { values, handleChange } = this.props;
    return (
      <MuiThemeProvider>
        <React.Fragment>
          <TextField
            style={styles.input}
            hintText="Enter Your Telephone Number"
            type='number'
            floatingLabelText="Tel."
            onChange={handleChange('telnum')}
            defaultValue={values.telnum}
          />
          {/* <br />
          <TextField
            style={styles.input}
            hintText="Enter Your City"
            floatingLabelText="City"
            onChange={handleChange('city')}
            defaultValue={values.city}
          />
          <br />
          <TextField
            style={styles.input}
            hintText="Enter Your Bio"
            floatingLabelText="Bio"
            onChange={handleChange('bio')}
            defaultValue={values.bio}
          /> 
          <br />
          <RaisedButton
            label="Back"
            primary={false}
            style={styles.button}
            onClick={this.back}
          />
          <RaisedButton
            label="Continue"
            primary={true}
            style={styles.button}
            onClick={this.continue}
          /> */}
        </React.Fragment>
      </MuiThemeProvider>
    );
  }
}

const styles = {
  button: {
    margin: 5,
    marginBottom: 10
  },
  input: {
    width: '95%', 
    marginLeft: 10, 
    marginRight: 10
  }
};
export default FormPersonalDetails;
