import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux'
import { firestoreConnect } from 'react-redux-firebase'
import Loading from '../LoadingComponent'
import renderHTML from 'react-render-html'
import { Card, Row} from 'antd'
import { Link } from 'react-router-dom'

const { Meta } = Card;

const RenderItem = (props) => {
    
        const {item, loading} = props;
        if(item){
            return(
            <div style={{ margin: 'auto', padding: '10px 15px ',  marginLeft: '0px', marginRight: '0px' }}>
                <Row style={{ padding: '0px',  marginLeft: '0px', marginRight: '0px' }}>
                    <Card
                        
                        hoverable
                         style={{ height: 300, width: 335, marginTop: '0px', marginBottom: '0px', marginLeft: '0px', marginRight: '0px',
                        overflow: 'hidden' }}
                        cover= {item.pictureUrl ? <div className="embed-responsive embed-responsive-16by9 mt-0 mb-0 pb-0">
                        <img src={item.pictureUrl} className="embed-responsive-item w-100" alt="NoImage" />
                      </div> : null}
                        >
                        <Meta 
                        description= {item.content? 
                            <Fragment>
                                <span style={{ color: 'indigo', marginTop: '-15px' }} className="h5">{item.title}</span> <br />
                         {renderHTML(item.content)}
                            </Fragment>
                          : null}
                        />
                    </Card>
                </Row>
            </div>
            )
        }
        else if(loading) {
            return(
                <div className="container section project-details mt-5 pt-5">
                <div className="card z-depth-0 col-12 col-md-8 mx-auto">
                <div className="spinner mx-auto pt-2">
                    <Loading />
                </div>
                </div>
            </div>
            )
        }
        else return (null);
}

class  Categry extends Component {

    render() { 
        return(
            <div className="row">
            <div className="politics">
                <Link to={'/politics'}>
                    <RenderItem className="politics" item={this.props.projects.filter((project) => project.politics)[0]} />
                </Link> 
            </div>
            <div className="sports">
                <Link to={'/sports'}>
                    <RenderItem className="sports" item={this.props.projects.filter((project) => project.sports)[0]} />
                </Link>
            </div>
            <div className="entertainment">
            <Link to={'/entertainment'}>
            <RenderItem className="entertainment" item={this.props.projects.filter((project) => project.entertainment)[0]} />
            </Link>
            </div>
            <div className="crime-watch">
            <Link to={'/crime-watch'}>
            <RenderItem className="crime-watch" item={this.props.projects.filter((project) => project.crimeWatch)[0]} />
            </Link>
            </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    // console.log(state);
    return {
      projects: state.firestore.ordered.projects,
      auth: state.firebase.auth,
      notifications: state.firestore.ordered.notifications
    }
}

// export default HomeComponent;
export default firestoreConnect([{ collection: 'projects', orderBy: ['createdAt', 'desc'], limit: 5 }])( connect( mapStateToProps )(Categry) )
