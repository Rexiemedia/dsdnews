import React from 'react'
import renderHTML from 'react-render-html'


const CarouselSummary = ({project, auth}) => {
    return (
        <div className="row mx-auto mt-3 mb-2 pl-2 pr-2">
            {project.pictureUrl ?             
            <div className="col-12 col-md-6 embed-responsive embed-responsive-16by9 mt-auto mb-auto">
              <img src={project.pictureUrl} className="embed-responsive-item w-100" alt={"NoImage"} />
            </div> : null}
           
            <div className="col-12 col-md-6 mt-auto mb-auto carousel-text">
                <p className="h4 text-left text-success">{project.title}</p> 
                <span className="content-carousel">{renderHTML(project.content)}</span>
            </div>
        </div>
    )
}
export default CarouselSummary;