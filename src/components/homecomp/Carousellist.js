import React from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { firestoreConnect } from 'react-redux-firebase'
import CarouselSummary from './carouselSummry'
import { Carousel } from 'antd';

const CaroulseltList = (props) => {
    const { projects, auth } = props;
    return(
        <Carousel autoplay effect="fade" className="row mt-2 mb-2">
            { projects && projects.map(project => {
                return (
                <Link to={'/project/' + project.id} key={project.id}>
                    <CarouselSummary project={project} auth={auth} />
                </Link>
                )
            })}  
        </Carousel>            
    )
}

const mapStateToProps = (state) => {
    return {
      projects: state.firestore.ordered.projects,
      auth: state.firebase.auth
    }
}

export default firestoreConnect([{ collection: 'projects', orderBy: ['createdAt', 'desc'], limit: 5 }])( connect( mapStateToProps )(CaroulseltList) )
