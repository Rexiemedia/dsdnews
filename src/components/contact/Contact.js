import React, { Component } from 'react'
import { connect } from 'react-redux'
import { contactUs } from '../store/actions/contactAction'
import UserForm from '../containers/UserForm'


class Contact extends Component {
  
render() {
     
    return ( 
            <div className="container mt-3 mb-4 project-details">
              <div className="col-12 col-md-6 bg-white justify-content-center pt-2 pl-2 pr-2 mx-auto rounded">
                <h3 style={{
                  backgroundColor: 'rgba(0, 188, 212)', 
                  padding: 18, 
                  color: '#fff', 
                  margin: -7,
                  borderTopRightRadius: 4,
                  borderTopLeftRadius: 4
                  }}>
                  Contact Us
                </h3>
                <UserForm  />
              </div>
           </div>
         );
         
    }
}


const mapDispatchToProps = dispatch => {
    return {
      contactUs: (contact) => dispatch(contactUs(contact))
    }
}
  
export default connect(null, mapDispatchToProps)(Contact)