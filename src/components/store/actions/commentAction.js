export const createComment = (comment) => {
    return (dispatch, getState, {getFirebase, getFirestore}) => {
      const firestore = getFirestore();
      const profile = getState().firebase.profile;
      const authorId = getState().firebase.auth.uid;
      const authorName = getState().firebase.auth.displayname;
      // eslint-disable-next-line
      const firstName = {firstName} ? profile.firstName : authorName.split(" ", 1);
      // eslint-disable-next-line
      const lastName = {lastName} ? profile.lastName :  authorName.split(" ", 2);
      
      firestore.collection('comments').add({
        ...comment,
        authorFirstName: firstName,
        authorLastName: lastName,
        authorId: authorId,
        createdAt: new Date()
      }).then(() => {
        dispatch({ type: 'CREATE_COMMENT_SUCCESS' });
      }).catch(err => {
        dispatch({ type: 'CREATE_COMMENT_ERROR' }, err);
      });
    }
  };
