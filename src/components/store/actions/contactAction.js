
export const contactUs = (contact) => {
    return (dispatch, getState, { getFirestore}) => {
      const firestore = getFirestore();
   
      firestore.collection('feedbacks').add({
        ...contact,
        createdAt: new Date()
      }).then(() => {
        dispatch({ type: 'FEEDBACK_SUCCESS' });
      }).catch(err => {
        dispatch({ type: 'FEEDBACK_ERROR' }, err);
      });
    }
  };