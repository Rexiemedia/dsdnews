
export const createProject = (project) => {
  return (dispatch, getState, {getFirebase, getFirestore}) => {
    const firestore = getFirestore();
    const profile = getState().firebase.profile;
    const authorId = getState().firebase.auth.uid;
    const {title, content, crimeWatch, entertainment,  politics, sports, pictureUrl} = project;

    firestore.collection('projects').add({
      title: title,
      content: content, 
      crimeWatch: crimeWatch, 
      entertainment: entertainment,  
      politics: politics, 
      sports: sports,
      pictureUrl: pictureUrl,
      authorFirstName: profile.firstName,
      authorLastName: profile.lastName,
      authorId: authorId,
      createdAt: new Date()
    }).then(() => {
      dispatch({ type: 'CREATE_PROJECT_SUCCESS' });
    }).catch(err => {
      dispatch({ type: 'CREATE_PROJECT_ERROR' }, err);
    });
  }
};

export const updateProject = (project) => {
  return (dispatch, getState, {getFirestore}) => {
    const firestore = getFirestore();
    const profile = getState().firebase.profile;
    const authorId = getState().firebase.auth.uid;
    // eslint-disable-next-line
    const id = id;
    firestore.collection(`projects/${id}`).set({
      ...project,
      authorFirstName: profile.firstName,
      authorLastName: profile.lastName,
      authorId: authorId,
      updatedAt: new Date()
    }).then((res) =>{
      console.log(res.data());

      firestore.collection(`notifications/${id}`).set({
        authorFirstName: profile.firstName,
        authorLastName: profile.lastName,
        authorId: authorId,
        updatedAt: new Date()
      })
    })
    .then(() => {
      dispatch({ type: 'UPDATE_PROJECT_SUCCESS' });
    }).catch(err => {
      dispatch({ type: 'UPDATE_PROJECT_ERROR' }, err);
    });
  }
};