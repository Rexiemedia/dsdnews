import authReducer from './authReducer'
import projectReducer from './projectReducer'
import commentReducer from './commentReducer'
import feedbackReducer from './feedbackReducer'
import { combineReducers } from 'redux'
import { firestoreReducer } from 'redux-firestore';
import { firebaseReducer } from 'react-redux-firebase'

const rootReducer = combineReducers({
  auth: authReducer,
  project: projectReducer,
  comment: commentReducer,
  contact: feedbackReducer,
  firestore: firestoreReducer,
  firebase: firebaseReducer
});

export default rootReducer

// the key name will be the data property on the state object