const initState = {}

const feedbackReducer = (state = initState, action) => {
  switch (action.type) {
    case 'FEEDBACK_SUCCESS':
      return state;
    case 'FEEDBACK_ERROR':
      return state;
    default:
      return state;
  }
};

export default feedbackReducer;