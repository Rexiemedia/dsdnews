import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import Notification from './Notifications'
import ProjectList from '../projects/ProjectList'
import { connect } from 'react-redux'
import { firestoreConnect } from 'react-redux-firebase'
import { compose } from 'redux'
import { Redirect } from 'react-router-dom'

class Dashboard extends Component {

    render() {
        const { projects, auth, notifications } = this.props;
        const {pathname} = this.props.location;
        let lastLocation = pathname;

        if (auth.uid !== '3rWOMvlI2vZqfuNCvPaWKe4CFi03') return <Redirect to='/signin' lastLocation={lastLocation}/> 

    return (
        <div className="container pt-2 project-details">
            <div className="row">
                <div className="col-12 col-sm-2">
                    <ul className="nav justify-content-start mb-2 bg-white">
                        <Link to='/createproject' className="nav-item nav-link">Create project</Link>
                        <Link to='/users' className="nav-item nav-link">Delete User</Link>
                        <Link to='/comments' className="nav-item nav-link">Delete Comment</Link>
                    </ul>
                </div>
                <div className="col-11 mx-auto col-md-7">
                    <ProjectList projects={projects} auth={auth} />
                </div>
                <div className="col-12 col-md-3">
                    <Notification notifications={notifications} auth={auth}  />
                </div>
            </div>
        </div>
    );
  }
}

const mapStateToProps = (state) => {
    // console.log(state);
    return {
      projects: state.firestore.ordered.projects,
      auth: state.firebase.auth,
      notifications: state.firestore.ordered.notifications
    }
  }
  
  export default compose(
    connect(mapStateToProps),
    firestoreConnect([
      { collection: 'notifications', orderBy: ['time', 'desc']},
      { collection: 'projects', orderBy: ['createdAt', 'desc']},
  
    ])
  )(Dashboard)
  
