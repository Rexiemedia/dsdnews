import React from 'react'
import moment from 'moment'
import { Link } from 'react-router-dom'


const Notification = (props) => {
const { notifications, auth } = props;
if( auth.uid === '3rWOMvlI2vZqfuNCvPaWKe4CFi03')
  return (
    <div className="section justify-content-center">
      <div className="card">
        <div className="card-content">
          <h4 className="card-title text-center">Latest News</h4>
          <ul className="list-group">
            { notifications && notifications.map(item =>{
              return <li className="list-group-item" key={item.id}>
              <Link  to={'/project/' + item.projectId}>
                <span className="text-success text-center">{moment(item.time.toDate()).fromNow()}</span> 
                <span className="h5 ml-2 text-center">{item.title}</span>
              </Link>
              <span className="h5 ml-2 text-center">{item.content} {item.user}</span>
              </li>
            })}
          </ul>
          <p  className="mt-auto pt-3"><Link to='/all_cat' className="bg-success text-white p-2 rounded">View all</Link></p>
        </div>
      </div>
    </div>
  )
  return(
    <div className="section justify-content-center">
    <div className="card">
    
        <h4 className="card-title text-center text-success">Latest News</h4>
        <ul className="list-group mx-auto">
        {notifications &&  notifications.filter((notification) => notification.projectId)
            .map(item =>{
            return <li className="list-group-item" key={item.id} >
              <Link  to={'/project/' + item.projectId}>
              <span className="text-success text-center">{moment(item.time.toDate()).fromNow()} :</span>
              <span className="h6 ml-2 text-center">{item.title}</span>
              </Link>
            </li>
          })}
          </ul>
          <p  className="mt-auto pt-3"><Link to='/all_cat' className="bg-success text-white p-2 rounded">View all</Link></p>
      </div>
    </div>    
  )
}

export default Notification
